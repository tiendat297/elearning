<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|max:255',
            'repassword' => 'required|same:password',
            'phone' => 'required',
            'address' => 'required',
        ];
    }

    /**
     * Custom messge for validate
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Tên không được bỏ trống',
            'name.min' => 'Tên phải có tối thiểu 6 kí tự ',
            'name.max' => 'Tên không quá 255 kí tự ',
            'email.required' => 'Email không được bỏ trống',
            'email.unique' => 'Đã tồn tại email trong hệ thống',
            'email.email' => 'Sai định dạng email',

            'password.required' => 'Mật khẩu không được bỏ trống',
            'password.min' => 'Mật khẩu phải có tối thiểu 6 kí tự ',
            'password.max' => 'Mật khẩu không quá 255 kí tự ',
            'repassword.required' => 'Mật khẩu không được bỏ trống',
            'password.same' => 'Nhập sai',
            'phone.required' => 'Số điện thoại không được bỏ trống',
            'phone.required' => 'Địa chỉ không được bỏ trống',
        ];
    }
}
