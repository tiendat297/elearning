<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Requestresetpassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |-------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */



    public function form(){
        return view('auth.passwords.reset');
    }

    public function laylaimk(Request $request){
       $email = $request -> email;
       $checkUser = User::where('email',$email) -> first();
       if(!$checkUser){
        session()->flash('email_fail','Nhập sai mật khẩu');
        return redirect() -> back() ;
       }

       $code = Hash::make(md5(time().$email));
       $checkUser -> code = $code;
       $checkUser -> time_code = Carbon::now();
       $checkUser -> save();
       session()->flash('email_fail','Mật khẩu đã được gửi vào email của bạn');
       $url = route('get.link.reset.pass',['code' => $code, 'email' => $email]);
       echo $url;
//        $data = [
//         'route'  => $url
//     ];
//     Mail::send('client\pages\linkpass', $data, function ($message) use($email) {

//        $message->from('Lethihang08111999@gmail.com', 'Công ty TNHH Ba Đờn');

//        $message->to($email,'Visitor' );
//        $message->subject('Công ty TNHH Ba Đờn gửi link lấy lại mật khẩu');

//    });
//        return back();


    }
    public function resetpass(Request $request){
        $code = $request -> code;
        $email = $request -> email;
        $checkUser = User::where([
            'code' => $code,
            'email' => $email
        ]
        ) -> first();
        if(!$checkUser){
            session()->flash('email_fail','Đường dẫn mật khẩu không đúng, vui lòng kiểm tra lại');
            return redirect('password');

        }
        return view('auth.passwords.confirm');
    }
    public function postresetpass(Requestresetpassword $request){
        if($request -> password){
            $code = $request ->code;
            $email = $request -> email;
            $checkUser = User::where([
                'code' => $code,
                'email' => $email,
            ]) -> first();
            if(!$checkUser){
                return redirect('password');
            }
            $checkUser -> password = Hash::make($request -> password);
            $checkUser -> save();
            session()->flash('email_fail','Đặt lại mật khâu');
            return redirect('password');
        }
    }
}
