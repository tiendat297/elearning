<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Routing\Route;
use Closure;
session_start();
class acessper
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { // điều kiện của middleware
        $admin_id = session()->get('admin_id');
        if($admin_id){
            return $next($request);
        }
        return redirect('login_admin');
    }
}
