@include('client.layouts.botton')


<div id="mainBody">
	<div class="container">
	<div class="row">
<!-- Sidebar ================================================== -->

<!-- Sidebar end=============================================== -->
	<div class="span12">
    <ul class="breadcrumb">
		<li><a href="index.html">Home</a> <span class="divider">/</span></li>
		<li class="active">Registration</li>
    </ul>
    @if (session('email_fail'))




	<div class="alert alert-block alert-error fade in">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>Cảnh báo:  {{ session('email_fail') }}</strong>
	 </div>


     @endif
	<h3> Nhập mật khẩu mới</h3>
<div class="well">
	<form class="form-horizontal" action="{{ url("postresetpass") }}" method="POST">
        @csrf
		<h4>Vui lòng nhập mật khẩu mới</h4>
		<div class="control-group">


		</div>


		<div class="control-group">
		<label class="control-label" for="input_email">Password <sup>*</sup></label>
		<div class="controls">
		  <input type="password" id="" placeholder="Password" name="password">
		</div>
	  </div>
      @if($errors -> has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors -> first('password') }}</strong>
                                    </span>
                                @endif
      <div class="control-group">
		<label class="control-label" for="input_email">Confirm Password <sup>*</sup></label>
		<div class="controls">
		  <input type="password" id="" placeholder="repassword" name="repassword">
		</div>
	  </div>
      @if($errors -> has('repassword'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors -> first('repassword') }}</strong>
                                    </span>
                                @endif




        <div class="control-group">
			<div class="controls">

				<input class="btn btn-large btn-success" type="submit" value="Lấy lại mật khẩu" />
			</div>
		</div>
	</form>
</div>

</div>
</div>
</div>
</div>
@include('client.layouts.footer')
